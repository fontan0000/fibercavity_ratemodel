
%%%%%%% Transfer Matrix for Fiber Cavity %%%%%%%%%%%%%%%%%%%%%%%


function [lambda,A,T]=transferM_FC(CL,thickness_membrane,w0,bw,res)


% Set range in wavelengths in nm, l_min and l_max and number of points in between
% res - if only one wavelength wanted set it in l_min and choose res = 1

l_min=w0-0.5.*bw;
l_max = w0+0.5.*bw;

%l_min = 610;
%l_max = 710;

res = 150;

% Set angular range (in degrees) from th_min to th_max and number of angles to calculate
% res_th - if only one angle wanted set it in th_min and choose res_th = 1

th_min = 0;
th_max = 89;

res_th = 1;

% Set thicknesses of layers in nm
%%%ttry to build iterative system:

%a block is a recursive set of layers or a single layer

%Nblocks=4 %4 is supermirror-diamond-air-supermirror

%flat supermirror
unit1={'Ti2O5_n.mat' 'SiO2_n.mat'};
d1=[74.6 102.7];
Block1={};
thickness1=[];
for i=1:13
    Block1=[Block1 unit1];
    thickness1=[thickness1 d1];
end

%diamond membrane
%Block2={'Diamond_n.mat'};
Block2={'self'};
thickness2=thickness_membrane;

%air gap
Block3={'Air_n.mat'};
thickness3=CL.*10^3;

%fibre supermirror
unit4={'Ti2O5_n.mat' 'SiO2_n.mat'};
d4=[74.6 102.7];
Block4={};
thickness4=[];
for i=1:12
    Block4=[Block4 unit4];
    thickness4=[thickness4 d4];
end

%packing altogether
%...


%%%%%


d = [thickness1 thickness2 thickness3 thickness4];

% Set name of input file located in /materials/ containg refractive index
% of cylinder (name1) and environment (name2)

% If you want to type in the refractive index of one layer (non-dispersive,
% but can be complex) tyle 'self' in the name of the layer and set n_self =
% ... as your refractive index


%name1 = {'Ti2O5_n.mat' 'SiO2_n.mat' 'Diamond_n.mat' 'air_n.mat' 'Ti2O5_n.mat' 'SiO2_n.mat'};
name1=[Block1 Block2 Block3 Block4];

%makeshift for diamond+broadening
%artificial dissipation:
k_art=-0.1j;
n_self = 2.4919+k_art;

% refractive index of surroundings (semi infinite media), must be
% transparent

n0 = 'SiO2_n.mat'; % incident medium
ne = 'SiO2_n.mat'; % exiting medium

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%% End of user's input %%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Thickness processing

d=d.*1e-9;

% Lambda processing

if res == 1
    lambda = l_min*1e-9;
else
    lambda = linspace(l_min*1e-9, l_max*1e-9, res);
    lambda = lambda';
end

% Theta processing

if res_th == 1
    theta0 = th_min*(pi/180);
else
    theta0 = (pi/180)*(linspace(th_min, th_max, res_th));
end

% Load environment's refractive index as above

ee0 = load(strcat('materials\',n0));  % loading antenna material complex n
l0=ee0.lambda;                     % load the wavelengths
n0_uninterp=ee0.n + 1i*ee0.k;            % load the dielectric constant
if size(n0_uninterp,1) == 1
    n0 = repmat(n0_uninterp, res,1);
else
     n0=spline(l0,n0_uninterp,lambda);  
     n0 = n0';
end

n = [n0];

% Load the layers' refractive index (complex, dispersive)

for g = 1:size(name1,2)
    
    if strcmp(name1{1,g}, 'self')==1 
        n1 = repmat(n_self, res,1);
    else    
ee1 = load(strcat('materials\',name1{1, g}));  % loading antenna material complex n
l1=ee1.lambda;                     % load the wavelengths
n1_uninterp=ee1.n - 1i*ee1.k;            % load the dielectric constant
if size(n1_uninterp) == 1
    n1 = n1_uninterp*ones(res, 1);
else
    n1=spline(l1,n1_uninterp,lambda);
end   



    end
    n = [n n1];
end
% Load exit environment's refractive index as above

ee0 = load(strcat('materials\',ne));  % loading antenna material complex n
l0=ee0.lambda;                     % load the wavelengths
ne_uninterp=ee0.n + 1i*ee0.k;            % load the dielectric constant
if size(ne_uninterp,1) == 1
    ne = repmat(ne_uninterp, res,1);
else
     ne=spline(l0,ne_uninterp,lambda);  
     ne = ne';
end

n = [n ne];

% number of layers

N = size(d,2);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Precondition for theta and calculations %%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

theta_l(:,:,1) = repmat(theta0, res, 1);

for z = 1:N+1
    
    theta_l(:,:,z+1) = asin((repmat(n(:,z), 1, res_th)./repmat(n(:,z+1), 1, res_th)).*sin(theta_l(:,:,z)));

end

theta_exit = abs(theta_l(:,:,N+2));
theta_l = theta_l(:,:,2:N+1);

for zz = 1:N
    
    kx(:,:,zz) = (2.*pi.*(repmat(n(:,zz+1), 1, res_th)).*cos(theta_l(:,:,zz)))./(repmat(lambda, 1, res_th));

end

for i = 1:res
    for j = 1:res_th
        
        D0_s{i,j} = [1 1; n(i,1)*cos(theta0(j)) -n(i,1)*cos(theta0(j))];
        D0_p{i,j} = [cos(theta0(j)) cos(theta0(j)); n(i,1)   (-n(i,1))];
        Dexit_s{i,j} = [1 1; n(i,N+2)*cos(theta_exit(i,j))   (-n(i,N+2)*cos(theta_exit(i,j)))];
        Dexit_p{i,j} = [cos(theta_exit(i,j)) cos(theta_exit(i,j)); n(i,N+2) (-n(i,N+2))];
        
        for k = 1:N
            
            D_s{i,j,k} = [1 1; n(i,k+1)*cos(theta_l(i,j,k)) (-n(i,k+1)*cos(theta_l(i,j,k)))];
            D_p{i,j,k} = [cos(theta_l(i,j,k)) cos(theta_l(i,j,k)); n(i,k+1) (-n(i,k+1))];
            P{i,j,k} = [exp(1i*d(k)*kx(i,j,k)) 0; 0 exp(-1i*d(k)*kx(i,j,k))];
            
            Mproduct_s{i,j,k} = D_s{i,j,k}*P{i,j,k}*(inv(D_s{i,j,k}));
            Mproduct_p{i,j,k} = D_p{i,j,k}*P{i,j,k}*(inv(D_p{i,j,k}));           
                          
        end
    end
end


if N == 1
    
    for ii = 1:res
        for jj = 1:res_th
            M_s{ii,jj} = (inv(D0_s{ii,jj}))*Mproduct_s{ii,jj}*Dexit_s{ii,jj};
            M_p{ii,jj} = (inv(D0_p{ii,jj}))*Mproduct_p{ii,jj}*Dexit_p{ii,jj};
            
            R_s(ii,jj) = (abs((M_s{ii,jj}(2,1))./(M_s{ii,jj}(1,1)))).^2;
            R_p(ii,jj) = (abs((M_p{ii,jj}(2,1))./(M_p{ii,jj}(1,1)))).^2;
            T_s(ii,jj)= ((n(ii,N+2)*cos(theta_exit(ii,jj)))./(n(ii,1)*cos(theta0(jj)))).*((abs(1./(M_s{ii,jj}(1,1)))).^2);
            T_p(ii,jj)= ((n(ii,N+2)*cos(theta_exit(ii,jj)))./(n(ii,1)*cos(theta0(jj)))).*((abs(1./(M_p{ii,jj}(1,1)))).^2);
        end
    end
    
    
else
    

for kk = 1:N 
    
    if kk == 1
        
        for ii = 1:res
            for jj = 1:res_th
                
                Loopproduct_s{ii,jj}= Mproduct_s{ii,jj,N+1-kk}*Dexit_s{ii,jj};
                Loopproduct_p{ii,jj}= Mproduct_p{ii,jj,N+1-kk}*Dexit_p{ii,jj};

            end
        end
        
    else
        if kk == N
            for ii = 1:res
                for jj = 1:res_th
                    M_s{ii,jj} = inv(D0_s{ii,jj})*Mproduct_s{ii,jj,N+1-kk}*Loopproduct_s{ii,jj};
                    M_p{ii,jj} = inv(D0_p{ii,jj})*Mproduct_p{ii,jj,N+1-kk}*Loopproduct_p{ii,jj};
                    
                    R_s(ii,jj) = (abs((M_s{ii,jj}(2,1))./(M_s{ii,jj}(1,1)))).^2;
                    R_p(ii,jj) = (abs((M_p{ii,jj}(2,1))./(M_p{ii,jj}(1,1)))).^2;
                    T_s(ii,jj)= ((n(ii,N+2)*cos(theta_exit(ii,jj)))./(n(ii,1)*cos(theta0(jj)))).*(abs(1./(M_s{ii,jj}(1,1))).^2);
                    T_p(ii,jj)= ((n(ii,N+2)*cos(theta_exit(ii,jj)))./(n(ii,1)*cos(theta0(jj)))).*(abs(1./(M_p{ii,jj}(1,1))).^2);
                end
            end
        else
            
            for ii = 1:res
                for jj = 1:res_th
                    Loopproduct_s{ii,jj}= Mproduct_s{ii,jj,N+1-kk}*Loopproduct_s{ii,jj};
                    Loopproduct_p{ii,jj}= Mproduct_p{ii,jj,N+1-kk}*Loopproduct_p{ii,jj};
                end
            end
        end
    end
end

end


R = 0.5.*(R_s+R_p);
T = 0.5.*(T_s+R_p);
A_s = 1-R_s-T_s;
A_p = 1-R_p-T_p;
A = 0.5.*(A_s+A_p);
lambda = lambda*1e9;
theta0 = theta0*(180/pi);

end