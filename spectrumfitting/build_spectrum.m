function [sumspec] = build_spectrum(data,scale,I,P,nrep)
%Fit the spectrum of a defect in diamond: a zero-phonon line and phonon
%sideband


%Define Lorentzien lineshape:
L=@(I,w0,g) I.*(0.5.*g).^2./((w0-scale).^2+(0.5.*g).^2);

%zero phonon line

spec(1,:)=L(I(1),P(1),P(2));
       
%Loop on the sidebands

        for i=1:nrep
            
            spec(i+1,:)=L(I(i+1),(P(1)+P(3)+(i-1).*P(4)),P(5));
            
        end
        
        sumspec=sum(spec,1)';
        
        %Optionally plot the result
        if 1
            
            plot(scale,data,'Color',[0.5 0.5 0.5],'LineWidth',2)
            hold on
            plot(scale,spec,'LineWidth',1)
            plot(scale,sumspec,'k','LineWidth',2)
            axis tight
            hold off
        end

end

