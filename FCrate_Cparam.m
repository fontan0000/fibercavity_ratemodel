function [P_tot,P] = FCrate_Cparam(CL_target,target_wl,R_c,CF,wl_rangeinput,factor_deph)

%modified version, input is the cavity parameters only

%speed of light c (nm/s)
c=299792458.*10^(9);

%Compute the corresponding cavity length CL corresponding to wlrange (without resonance tracker)
wl_range=wl_rangeinput;
%wl_range=(299792.45800)./469.6;
%rescale CL and the radius of curvature to nm:
n=2.*CL_target./target_wl;
CL=0.5.*n.*wl_range.*10^3;
R_c=R_c.*10^3;
%NV spectrum, decomposed in ZPL and PSB contributions

%peaks positions in (T)Hz
%for NV centers
%x_peaks=[469.6 461.9 452.6 439.7 429.0 416.2 403.2 392.4].*10^12;

%for GeV centers
x_peaks=[497.5934 494.0270 487.9495 481.8720 475.7946 469.7171 463.6397 457.5622].*10^12;

%peaks width in (T)Hz
%for NV centers
%w_peaks=[factor_deph.*2.44 15.9 15.5 15.0 16.5 12.7 13.7 16.1].*10^12;
%w_peaks(1)=w_peaks(1);

%for GeV centers
w_peaks=[factor_deph.*4.94 7.773 7.773 7.773 7.773 7.773 7.773 7.773].*10^12;
w_peaks(1)=w_peaks(1);

%peak areas
%for NV centers
%A_peaks=[1520 5260 18600 16400 14000 9180 6570 3270].*10^12;

%for GeV centers
A_peaks=[5.3898 4.2139 3.9459 1.6050 1.0574 1.0235 0.5439 0.2412].*10^12;

%Relative peaks strength:
tic
    for i=1:size(x_peaks,2)
        RS_peaks(i)=A_peaks(i)./sum(A_peaks);
    end
    
%rates, related to NV-
%total radiative rate (est. from lifetime in bulk, MHz)

%For NVs centers
%y_tot=83.3.*10.^6; %modified with the 2Pi factor
%For GeV centers
y_tot=26.*2.*pi.*10.^6; %modified with the 2Pi factor

%radiative rates for each n-phonon transitions (not necessary??)
  %  for i=1:size(A_peaks,2)
   %     y_i(i)=y_tot.*RS_peaks(i);
   % end
   
   
%pure dephasing broadening, common to all:
gammastar=2.*pi.*w_peaks(1);

%phonon-induced dephasing (0 for the ZPL)
gamma_phonon= 2.*pi.*w_peaks-gammastar;




    for i=1:size(CL,2)

        %get the resonant wl corresponding to CL
        wl=wl_range(i);
        
        %Details of the cavity for a certain CL-WL couple (wl in nm) , calculations of factors:
        FSR=c./(2.*CL(i));

        %loss rate
        kappa=2.*pi.*FSR./CF;  %modified with the 2Pi factor

        %mode waist and mode volume V_mod:
        waist=sqrt(wl./pi.*sqrt(CL(i).*(R_c-CL(i))));
        V_mod=pi./4.*waist.^2.*CL(i);

        %Coupling strength, global g, and for each n-phonon transitions:
        g=sqrt((3.*c.*wl.^2.*0.5.*y_tot)./(4.*pi.*V_mod));
        
        
        %Calculation of the rates for each n-phonon transitions F(CL,...)
            for n=1:size(x_peaks,2)
                
                %Coupling strength of each n-phonon transition
                g_transition=g.*sqrt(RS_peaks(n));
                
                %detuning of each n-phonon transition, w/respect to the
                %resonant wl
                detuning=(299792.45800.*10^12)./wl-x_peaks(n);
                
                %rate of the nth phonon transition
                gamma_sum=(kappa+gammastar+y_tot+gamma_phonon(n));
                R(n,i)=(4.*g_transition.^2)./gamma_sum.*1./(1+((2.*2.*pi.*detuning)./gamma_sum).^2); % I put the 2Pi factor to straigthen the frequency units (matching the angular frequency w of the rates)
                
                Gamma_rec(i,n)=gamma_sum;
                g_rec(i,n)=g_transition;
                I_max(i,n)=(4.*g_transition.^2)./(kappa+gammastar+y_tot+gamma_phonon(n));
            end
            
            
    end
    time=toc;
    %disp(strcat('Rates and probabilities calculated in: ',num2str(time)))

    P=(R./y_tot.*(1+R./y_tot).^-1).*100;
    
    P_tot=sum(P,1);
    
    %printing out the fitted spectrum:
    if 1
        fitrec_spec=@(lambda) specbuilder(lambda,A_peaks,w_peaks,x_peaks);
        spectrum_ext=fitrec_spec(wl_range);
        renorm_coef=integral(fitrec_spec,0,1050);
        
        figure
        subplot(7,1,[1:4])
        h=area(wl_range,spectrum_ext./renorm_coef.*100);
        h.FaceColor=[0.98 1 1];
        h.EdgeColor=[0.6 0.7 0.8];
        h.LineWidth=1.5;
        h.ShowBaseLine='off';

        hold on

        plot(wl_range,P_tot,'Color',[0,0,0],'LineWidth',2)

        hold off
        ylabel('Intensity (% tot. em.)');

        ylim([-0.05.*max(P_tot) 1.1.*max(P_tot)])

        set(gca,'fontsize', 12);

        subplot(7,1,[6:7])
        hold on
        plot(wl_range,P_tot,'Color',[0,0,0],'LineWidth',2)

            for l=1:size(x_peaks,2)
                k=9-l;
                h=area(wl_range,P(k,:));
                h.FaceColor=[1 1 1];
                h.EdgeColor=[0.25+0.1.*(k-1)./7 0.25+0.1.*(k-1)./7 0.5+0.5.*(k-1)./7];
                h.FaceAlpha = 0.7;
                h.LineWidth=1.5;
                h.ShowBaseLine='off';
            end

            xlabel('Wavelength (nm)');
            %ylabel('Intensity (a.u.)');

            ylim([-0.05.*max(P_tot) 1.1.*max(P_tot)])
            hold off
            set(gca,'fontsize', 12);
    end
    
    if 0
        figure
        for n=1:8
            
            subplot(1,3,1)
            hold on
            plot(wl_range,g_rec(:,n),'Color',[(8-n)./7 0 (n-1)./7])
            axis tight
            subplot(1,3,2)
            hold on
            plot(wl_range,Gamma_rec(:,n),'Color',[(8-n)./7 0 (n-1)./7])
            axis tight
            subplot(1,3,3)
            hold on
            plot(wl_range,I_max(:,n),'Color',[(8-n)./7 0 (n-1)./7])
            axis tight
        end
    end
    
    %Ancillary functions, building spectra (SPECBUILDER)
    
    function [exp_fittedspec]=specbuilder(wl_range,A_peaks,w_peaks,x_peaks)
            exp_fittedspec=zeros(size(wl_range));
            
            for k=1:size(x_peaks,2)
                exp_fittedspec=exp_fittedspec+(2.*A_peaks(k))./pi.*w_peaks(k)./(4.*(((299792.45800.*10^12)./wl_range)-x_peaks(k)).^2+w_peaks(k).^2);
                %adapted to GeV datas:
                
            end  
    end
    

    
end

