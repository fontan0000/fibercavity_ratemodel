function [l_optim] = FindCavityLength(target_m,target_v,diamond_thickness)

%parameters and corresponding target value for the researched cavity length
m=target_m;
v=target_v; %in TeraHerz
td=diamond_thickness; %in microns

%invariable paramenters:
c=299792458;
nd=2.41;

%prep. equation
   W_approx_full=@(l,m,td) c./(2.*pi.*(l+(nd-1).*td).*1e-6).*(pi.*m-(-1).^m.*asin((nd-1)./(nd+1).*sin(m.*pi.*(l-(nd+1).*td)./(l+(nd-1).*td))))*1e-12;
   %W_approx_full=@(l,m,td) c./(2.*pi.*(l+(nd-1).*td)).*(pi.*m-(-1).^m.*asin((nd-1)./(nd+1).*sin(m.*pi.*(l-(nd+1).*td)./(l+(nd-1).*td))));
%Optimization function:

OptimLFun=@(l) v-W_approx_full(l,m,td);

%Estimated length
l0=m.*c./(2.*v.*1e12).*1e6;


%Solving:
l_optim=fsolve(OptimLFun,l0);


l_optim;

end