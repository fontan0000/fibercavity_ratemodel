function [reso_wl,lambda_init,spec_init]=resonance_tracker(CL)


%start central wl and bandwidth (looking for cav. res.)

cw_0=620; %good compromise
bw_0=15;

resonance=cw_0;

res_0=400;

%stop crit:
eps=100;
shunt=0;

    for i=1:size(CL,2)
        res=res_0;
        [lambda,~,T]=transferM_FC(CL(i),1,resonance,bw_0,res);
        resonance=lambda(find(T==min(T)));
        bw=bw_0;
        
        %first spec
        spec_init(:,i)=T;
        lambda_init(:,i)=lambda;
        
            while(eps>1e-6)
                    [lambda,~,T]=transferM_FC(CL(i),1,resonance,bw./10,res./4);

                    bw=bw./10;
                    res=res./4;
                    
                    resonance_prev=resonance;
                    resonance=lambda(find(T==min(T)));
                    
                    eps=abs((resonance-resonance_prev)./resonance);
                    
                    
                    
                    shunt=shunt+1;
                    
                        if shunt>1000
                            btreak;
                            disp('not converged \n')
                        end
                    

            end
        
            reso_wl(i)=resonance;
        
            
    end

end