function [eff]=coupling(L_cv,R_cv,wlv)

    for i=1:max(size(L_cv))
        L_c=L_cv(i);
        for j=1:max(size(R_cv))
            R_c=R_cv(j);
            for k=1:max(size(wlv))
                wl=wlv(k);
                % passing the wl in microns (consist with the cavity length)
                wl=wl.*10^-3;
                %wf is radius mode fiber
                wf=2;
                %index (approx) core of fiber
                n_core=1.456;
                %waist of the cavity at minimum:        
                w0=sqrt(wl./pi.*sqrt(L_c.*(R_c-L_c)));

                %waist at the mirror (divided by 2, cause wf is the radius...(check that))
                w=0.5.*w0.*sqrt(1+(L_c.*wl./(pi.*w0.^2)).^2);

                %coupling efficiency (hunger 2010 review)
                eff(i,j,k)=4.*((wf./w+w./wf).^2+(pi.*n_core.*wf.*w./(wl.*R_c)).^2).^-1;
            end
        end
    end
eff=squeeze(eff);
end