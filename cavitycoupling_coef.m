function [g]=cavitycoupling_coef(CL_target,target_wl,R_c,CF,wl_rangeinput)


%speed of light c (nm/s)
c=299792458.*10^(9);

%Compute the corresponding cavity length CL corresponding to wlrange (without resonance tracker)
wl_range=wl_rangeinput;
%wl_range=(299792.45800)./469.6;
%rescale CL and the radius of curvature to nm:
n=2.*CL_target./target_wl;
CL=0.5.*n.*wl_range.*10^3;
R_c=R_c.*10^3;

     for i=1:size(CL,2)

            %get the resonant wl corresponding to CL
            wl=wl_range(i);

            %mode waist and mode volume V_mod:
            waist=sqrt(wl./pi.*sqrt(CL(i).*(R_c-CL(i))));
            V_mod=pi./4.*waist.^2.*CL(i);

            %Coupling strength, global g, and for each n-phonon transitions:
            g=sqrt((3.*c.*wl.^2.*0.5.*y_tot)./(4.*pi.*V_mod));
     end



end