function [P_tot,P] = FCrate_realCL(CF,CL,R_c)

%speed of light c (nm/s)
c=299792458.*10^(9);

%Compute the resonant wl corresponding to CL (with resonance tracker)
disp('Getting corresponding wavelength\n')
tic
[resonant_wl,~,~]=resonance_tracker(CL);
time=toc;
disp(strcat('Wavelength calculated in: ',num2str(time)))

%rescale CL and the radius of curvature to nm:
CL=CL.*10^3;
R_c=R_c.*10^3;
%NV spectrum, decomposed in ZPL and PSB contributions

%peaks positions in (T)Hz

x_peaks=[469.6 461.9 452.6 439.7 429.0 416.2 403.2 392.4].*10^12;

%peaks width in (T)Hz

w_peaks=[2.44 15.9 15.5 15.0 16.5 12.7 13.7 16.1].*10^12;

%peak areas

A_peaks=[1520 5260 18600 16400 14000 9180 6570 3270];

%printing out the fitted spectrum:
spectrum_ext=zeros(size(linspace(610,840,500)));
spectrum_res=zeros(size(resonant_wl));
    for k=1:size(x_peaks,2)
        spectrum_ext=spectrum_ext+(2.*A_peaks(k))./pi.*w_peaks(k)./(4.*(((299792.45800.*10^12)./linspace(610,840,500))-x_peaks(k)).^2+w_peaks(k).^2);
        spectrum_res=spectrum_res+(2.*A_peaks(k))./pi.*w_peaks(k)./(4.*(((299792.45800.*10^12)./resonant_wl)-x_peaks(k)).^2+w_peaks(k).^2);
    end
figure
plot(linspace(610,840,500),spectrum_ext,'-k')
hold on
plot(resonant_wl,spectrum_res,'-k','LineWidth',2)

%Relative peaks strength:
tic
    for i=1:size(x_peaks,2)
        RS_peaks(i)=A_peaks(i)./sum(A_peaks);
    end
    
%rates, related to NV-
%total radiative rate (est. from lifetime in bulk, MHz)
y_tot=83.3.*10.^6;

%radiative rates for each n-phonon transitions (not necessary??)
  %  for i=1:size(A_peaks,2)
   %     y_i(i)=y_tot.*RS_peaks(i);
   % end
   
   
%pure dephasing broadening, common to all:
gammastar=2.*pi.*w_peaks(1);

%phonon-induced dephasing (0 for the ZPL)
gamma_phonon= 2.*pi.*w_peaks-gammastar;




    for i=1:size(CL,2)

        %get the resonant wl corresponding to CL
        wl=resonant_wl(i);
        
        %Details of the cavity for a certain CL-WL couple (wl in nm) , calculations of factors:
        FSR=wl./(2.*CL(i));

        %loss rate
        kappa=FSR./CF;

        %mode waist and mode volume V_mod:
        waist=sqrt(wl./pi.*sqrt(CL(i).*(R_c-CL(i))));
        V_mod=pi./4.*waist.^2.*CL(i);

        %Coupling strength, global g, and for each n-phonon transitions:
        g=sqrt((3.*c.*wl.^2)./(V_mod));
        
        
        %Calculation of the rates for each n-phonon transitions F(CL,...)
            for n=1:size(x_peaks,2)
                
                %Coupling strength of each n-phonon transition
                g_transition=g.*sqrt(RS_peaks(n));
                %detuning of each n-phonon transition, w/respect to the
                %resonant wl
                detuning=(299792.45800.*10^12)./wl-x_peaks(n);
                
                %rate of the nth phonon transition
                
                R(n,i)=(4.*g_transition.^2)./(kappa+gammastar+y_tot+gamma_phonon(n)).*1./(1+((2.*detuning)./(kappa+gammastar+y_tot+gamma_phonon(n))).^2);
                
            end
            
            
    end
    time=toc;
    disp(strcat('Rates and probabilities calculated in: ',num2str(time)))

    P=R./y_tot.*(1+R./y_tot).^-1;
    
    P_tot=sum(P,1);
    
    plot(resonant_wl,P_tot,'.r')
    
    hold off
    
end

